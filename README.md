# Overview

Are you tired of hard to tune, over priced, over complicated analytics platforms? [Saiku Analytics Enterprise Edition](http://meteorite.bi/products/saiku) provides a flexible experience that allows you to dig deeper into your data without writing complex SQL queries. Find the data points you need efficently and intuitively with our web based, drag and drop interface. Leverage the power of Juju to connect to different data sources with zero configuration, enabling you to discover data, faster.

## Analyse and explore data. Wherever it is stored.

Saiku connects to a wide range of data sources allowing you to explore the data in real-time directly from the source.

Leveraging Juju, it enables Saiku to connect to:

* MySQL
* PostgreSQL
* Apache Drill
* Apache Hadoop

If you have other datasources including:

* MSSQL
* Vertica
* DB2
* ExSOL

and more. You can connect to these manually to provide the same level of exploration.

## User driven dashboarding.

Give users the ability to create their own dashboards from Saiku reports. Using the Saiku Dashboard Designer users can build and deploy their own flexible, parameter driven dashboards, without writing a single line of code. Filter reports in unison with combined filters, show the data your users want with the minimum of fuss.

## Give Saiku to everyone.

Saiku is designed to be as easy to deploy as it is to use. Saiku is 100% thin client. It works on any modern browser on PC and Mac. Saiku can easily be integrated into existing security frameworks and is optimised to run on commodity server hardware even with large user communities. Intelligent caching reduces the performance impact on the underlying database and minimises network traffic.

# Usage

## Recommended

To get started using Saiku Analytics we recommend using the Saiku Enterprise with MySQL or Saiku Enterprise with Apache Drill bundles. These enable an easy way to get a fully configured Saiku system up and running in a single command. 

Find out more here:

[Saiku with MySQL](https://jujucharms.com/u/spiculecharms/saiku-mysql/)
[Saiku with Drill](https://jujucharms.com/u/spiculecharms/saiku-drill/)


## Manual

How to deploy this charm:

    juju deploy ~spiculecharms/saiku-enterprise
    juju expose saiku-enterprise

You can also use a proxy in front of Saiku with the following:

    juju deploy ~spiculecharms/saiku-enterprise
    juju deploy haproxy
    juju add-relation saiku-enterprise haproxy
    juju expose haproxy

## Support

Saiku Enterprise is commerically licensed. The charm will allow a single user experience, for multiple users please enable an SLA and input your credit card details. At that point you will be charged per user.

To enable an SLA follow these instructions:


    juju sla essential --budget 100

Where 100 is your budget. You can also set different SLA levels, standard and advanced.

To find out more about how to use Saiku Analytics you can view our wiki at [http://wiki.meteorite.bi]


# Configuration

Default login is admin/admin, this should be changed within the administration console once you have logged in.

The Saiku Charm has a number of helper actions to let you deploy a fully operational server programatically. These are entirely optional and you can perform the same actions manually.

* Add A Schema

To connect to a data source you need 2 things a Data Source connection and also a Schema definition that describes the table structure to the OLAP engine. To design schema you can find out more [here](http://wiki.meteorite.bi/display/SAIK/Schema).

    juju action do saiku/0 addschema name=spark content="$(cat ${MYDIR}/../var/spark_schema.xml)"

* Add A Data Source

Along with a schema you require a data source that defines the JDBC connection and the schema to use with that connection.

    juju action do saiku/0 adddatasource content="type=OLAP\nname=taxi\ndriver=mondrian.olap4j.MondrianOlap4jDriver\nlocation=jdbc:mondrian:Jdbc=jdbc:hive2://${SPARK_PRIVATE_IPADDRESS}:10000;Catalog=mondrian:///datasources/spark.xml;JdbcDrivers=org.apache.hive.jdbc.HiveDriver;\nusername=admin\npassword=admin\n"

* Add A report

You can prepopulate the Saiku repository with reports with the following action.

    juju action do saiku/0 addreport content="$(cat ${MYDIR}/../var/demo_1.saiku)" path="/homes/home:admin/demo_1.saiku"

* Warm Cache

Saiku makes extensive use of caching to speed up response times especially over large and/or slow data sets. You can use this action to "warm the cache", so that users who login and run a query that can make use of the resultset this query provides will get a near instant response instead of the server having to read from the database. You can warm the cache with as many queries as you like.
 
    juju action do saiku/0 warmcache cubeconnection=taxi-mongo cubecatalog="Taxi Fares" cubeschema="Taxi Fares" cubename="Fares" query="WITH SET [~ROWS] AS {[Fares].[Payment Type].[Payment Type].Members} SELECT NON EMPTY {[Measures].[Max Tip Amount]} ON COLUMNS, NON EMPTY [~ROWS] ON ROWS FROM [Fares]"


# Contact Information

- [Main Website](http://meteorite.bi)
- [User mailing list](https://groups.google.com/a/saiku.meteorite.bi/forum/#!forum/user)
- [Saiku on Freenode IRC](http://irc.lc/freenode/%23saiku/t4nk@)